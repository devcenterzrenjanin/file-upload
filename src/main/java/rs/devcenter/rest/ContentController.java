package rs.devcenter.rest;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.devcenter.util.FileHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;

@Controller
@RequestMapping("/content")
public class ContentController {

    @RequestMapping(value = "/{file:.+}", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response, @PathVariable String file) throws IOException {
        File serverFile = new File(FileHandler.PICTURE_STORAGE_PATH + File.separator + file);
        String mimeType = URLConnection.guessContentTypeFromName(serverFile.getName());
        if (mimeType == null) {
            System.out.println("mime type is not detectable, will take default");
            mimeType = "application/octet-stream";
        }

        response.setContentType(mimeType);

        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + serverFile.getName() + "\""));

        response.setContentLength((int) serverFile.length());

        InputStream inputStream = new BufferedInputStream(new FileInputStream(serverFile));

        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

}
