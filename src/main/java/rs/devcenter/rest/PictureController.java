package rs.devcenter.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import rs.devcenter.common.PictureDto;
import rs.devcenter.service.PictureService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PictureController {

    private PictureService pictureService;

    public PictureController(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    @RequestMapping("/pictures")
    public ResponseEntity<List<PictureDto>> findAll() {
        List<PictureDto> pictures = pictureService.findAll();

        if (pictures == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(pictures, HttpStatus.OK);
    }

    @RequestMapping("/pictures/upload")
    public void uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("picture") String pictureString) throws IOException {
        pictureService.save(new ObjectMapper().readValue(pictureString, PictureDto.class), file);
    }

}
