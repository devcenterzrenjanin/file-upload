package rs.devcenter.common;

import org.jtransfo.DomainClass;
import rs.devcenter.domain.Picture;

@DomainClass(domainClass = Picture.class)
public class PictureDto {

    private Long id;
    private Long version;
    private String title;
    private String description;
    private String pictureUrl;

    public PictureDto() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
