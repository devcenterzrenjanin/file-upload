package rs.devcenter.util;

import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;

public class FileHandler {

    public static final String PICTURE_STORAGE_PATH = "D:\\Programming\\Temporary";
    public static String SERVER_HOSTNAME = "http://localhost";
    public static int SERVER_PORT = 8080;

    public FileHandler(Environment env) {
        SERVER_PORT = Integer.parseInt(env.getProperty("server.port"));
        SERVER_HOSTNAME = InetAddress.getLoopbackAddress().getHostAddress();
    }

    /**
     * Handles file saving on a server storage
     * @param mFile - MultipartFile from which bytes are extracted
     * @param fileName - Name of the file stored on the server
     * @throws IOException - In case that storage folder can't be created or file can't be saved
     */
    public static void storePicture(MultipartFile mFile, String fileName) throws IOException {
        // Storing bytes from MultipartFile
        byte[] bytes = mFile.getBytes();

        // Creating directories where to store pictures in case they don't exist
        File dir = new File(PICTURE_STORAGE_PATH);
        if (!dir.exists())
            dir.mkdirs();

        // Saving file on the server
        File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName + extractFileFormat(mFile.getOriginalFilename()));
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
        stream.write(bytes);
        stream.close();
    }

    /**
     * Extracts the file extension from the original file name
     * @param originalFileName - Original file name with extension before uploading
     * @return - Extracted extension ( i.e. .jpg, .png, etc. )
     */
    private static String extractFileFormat(String originalFileName) {
        return originalFileName.substring(originalFileName.lastIndexOf('.'));
    }
}
