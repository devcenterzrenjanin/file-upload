package rs.devcenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.devcenter.domain.Picture;


public interface PictureRepository extends JpaRepository<Picture, Long> {



}
