package rs.devcenter.service.impl;

import org.jtransfo.JTransfo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import rs.devcenter.common.PictureDto;
import rs.devcenter.domain.Picture;
import rs.devcenter.repository.PictureRepository;
import rs.devcenter.service.PictureService;
import rs.devcenter.util.FileHandler;

import java.io.*;
import java.util.List;

@Service
public class PictureServiceImpl implements PictureService {

    private PictureRepository pictureRepository;
    private JTransfo jTransfo;

    public PictureServiceImpl(PictureRepository pictureRepository, JTransfo jTransfo) {
        this.pictureRepository = pictureRepository;
        this.jTransfo = jTransfo;
    }

    @Override
    public List<PictureDto> findAll() {
        return jTransfo.convertList(pictureRepository.findAll(), PictureDto.class);
    }

    @Override
    public PictureDto findOne(Long id) {
        return jTransfo.convertTo(pictureRepository.findOne(id), PictureDto.class);
    }

    @Override
    public PictureDto save(PictureDto pictureDto, MultipartFile mFile) throws IOException {
        if (!mFile.isEmpty()) {
            FileHandler.storePicture(mFile, pictureDto.getTitle());
        }
        Picture picture = jTransfo.convertTo(pictureDto, Picture.class);
        return jTransfo.convertTo(pictureRepository.save(picture), PictureDto.class);
    }
}
