package rs.devcenter.service;


import org.springframework.web.multipart.MultipartFile;
import rs.devcenter.common.PictureDto;

import java.io.IOException;
import java.util.List;

public interface PictureService {

    List<PictureDto> findAll();

    PictureDto findOne(Long id);

    PictureDto save(PictureDto pictureDto, MultipartFile mFile) throws IOException;

}
