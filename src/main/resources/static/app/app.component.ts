import {Component} from "@angular/core";
import {Http} from "@angular/http";
import {Picture} from "./_models/picture";

@Component({
    selector: 'app-root',
    templateUrl: 'app/app.component.html'
})
export class AppComponent {
    picture: Picture = new Picture();
    file: File;
    error: boolean = false;
    errorMessage: string = "";

    constructor(private http: Http) {

    }

    submitFile() {
        this.picture.pictureUrl = "/content/" + this.picture.title +  this.file.name.substr(this.file.name.lastIndexOf('.'));
        let formData = new FormData();
        formData.append("file", this.file);
        formData.append("picture", JSON.stringify(this.picture));
        this.http.post("http://localhost:8080/api/pictures/upload", formData).subscribe((response) => {
            console.log(response.statusText);
        });
    }

    onAddFile(event) {
        this.file = event.target.files[0];
    }
}