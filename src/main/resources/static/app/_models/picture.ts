export class Picture {
    id: number;
    version: number;
    title: string;
    description: string;
    pictureUrl: string;
}