## Simple File Upload using Spring Framework and Multipart Files

In order to run this example, user is required to setup a `MySQL` database and user to access it with the same names as 
stated in application.yml configuration file. Before running the example, make sure that you have installed `nodejs` 
dependencies and compiled the `Typescrip` files.

Projects is developed using Spring Boot and Angular 4 frameworks.